#include <string>
#include <iostream>
#include "stack.h"
#include <vector>

using namespace std;

int Stack::load( string word ) {

    for( int i = 0; i < word.length(); i++ ) {

        if( ( word[i] >= 'a' && word[i] <= 'z' ) || ( word[i] >= 'A' && word[i] <= 'Z' ) ) {

            this->stack.push_back( tolower( word[i] ) );

        }
    }

    return 0;
}

int Stack::show() {

    for( int i = 0; i < this->stack.size(); i++ ) {
        cout << "[" << i << "]" << " -> " << this->stack[i] << endl;
    }

    return 0;
}

int Stack::compare( vector<char> secondStackToCompare ) {

    for( int i = 0; i < secondStackToCompare.size(); i++ ) {
        if( this->stack[i] != secondStackToCompare[i] ){
            return 500;
        }
    }

    return 0;
}

int Stack::erase() {
    this->stack.erase( this->stack.begin(), this->stack.end() );

    return 0;
}
