#include <vector>
#include <string>

using namespace std;

class Stack
{
    public:
        int compare( vector<char> secondStackToCompare );
        int load( string word );
        int erase();
        int show();
        
        vector<char> stack;
};
