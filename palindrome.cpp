#include <string>
#include <iostream>
#include <fstream>
#include "stack.h"

using namespace std;

int main()
{
    char input;
    string word;
    string reverseWord;
    Stack stackOne, stackTwo;

    while( input != 'x' )
    {
        cout << "a -> Vytvor dva nove a prazdne zasobniky" << endl;
        cout << "b -> Vyprazdni obsah oboch zasobnikov" << endl;
        cout << "c -> Nacitaj novy retazec z klavesnice do prveho zasobnika a zapis otocene aj do druheho" << endl;
        cout << "d -> Vypis obsah prveho zasobnika" << endl;
        cout << "e -> Vypis obsah druheho zasobnika" << endl;
        cout << "f -> Urci ci dany retazec je palindrom" << endl;
        cout << "x -> Ukonci program." << endl;
        cout << "Set your option: ";
        cin >> input;
        cout << endl << endl;

        switch( input )
        {
            case 'a':
                break;

            case 'b':
                stackOne.erase();
                stackTwo.erase();
                break;

            case 'c':
                cout << "Write your string to compare: ";
                cin >> ws;
                getline(cin, word);

                reverseWord = string( word.rbegin(), word.rend() );
                stackOne.load( word );
                stackTwo.load( reverseWord );

                break;

            case 'd':
                stackOne.show();
                break;

            case 'e':
                stackTwo.show();
                break;

            case 'f':
                if( stackOne.compare( stackTwo.stack ) == 0 ){
                    cout << "Je palindrom !!!" << endl << endl;
                }
                else {
                    cout << "Nieje palindrom !!!" << endl << endl;
                }

                break;

            case 'x':
                cout << "Bye Bye" << endl;
                break;

            default:
                cout << "You set unsupported option" << endl;
                break;
        }
    }
}
